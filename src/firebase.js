import firebase from 'firebase/app';
import 'firebase/firestore'
import 'firebase/auth'
  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  var firebaseConfig = {
    apiKey: "AIzaSyBRtNzblNHRMsV1NzLVJ5d-zENX0g8lmzk",
    authDomain: "login-react-e6bb6.firebaseapp.com",
    projectId: "login-react-e6bb6",
    storageBucket: "login-react-e6bb6.appspot.com",
    messagingSenderId: "410312697904",
    appId: "1:410312697904:web:4b0ee2c78e8cd71d8125f5",
    measurementId: "G-VQQJ6YXF32"
  };
  // Initialize Firebase
  const fb = firebase.initializeApp(firebaseConfig);
  //export const db = fb.firestore();
  export default fb;