import React,{useState, useEffect} from 'react';
import './assets/css/App.css';
import Login from "./components/login";
import Register from "./components/register";
import ImageForm from "./components/imageForm";
import fb from "./firebase";

function App (){   

  const [card,setLoginCard] = useState(true);
  const [user, setUser] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');

 
  const initialFormValues = {
    urlImagen:'',
    descripcion:''
  }

  const [values, setValues]= useState(initialFormValues);
  
  const changeCardState =()=>{
    setLoginCard(!card);
  };

  const clearInputs = () => {
    setEmail('');
    setPassword('');
  };

  const clearErrors = () => {
    setEmailError('');
    setPasswordError('');
  };

  const handleLogin = () =>{
    clearErrors();
    fb
      .auth()
      .signInWithEmailAndPassword(email,password)
      .catch(err=>{
        switch(err.code){
          case "auth/invalid-email":
          case "auth/user-disabled":
          case "auth/user-not-found":
            setEmailError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
          default:
            console.log("no hay ningun error");
        }
      });
  };

  const handleRegister = () =>{
    fb
      .auth()
      .createUserWithEmailAndPassword(email,password)
      .catch(err=>{
        switch(err.code){
          case "auth/email-already-in-use":
          case "auth/invalid-email":
            setEmailError(err.message);
            break;
          case "auth/wrong-password":
            setPasswordError(err.message);
            break;
          default:
            console.log("no hay ningun error");
        }
      });
  };

  const handleLogout=()=>{
    fb.auth().signOut();
  };

  const authListener = ()=>{
    fb.auth().onAuthStateChanged((user) => {
      if (user){
        clearInputs();
        setUser(user);
      } else {
        setUser('');
      }
    });
  };

  useEffect (() => {
    authListener();
  } );
  
  const handleSubmit = () =>{
    addImages(values);
  }

  const handleInputChange = (e) =>{
    const {name,value} = e.target;
    setValues({...values, [name]:value})
    }
  
  const addImages= async (links) =>{
    await fb.firestore().collection('Images').doc().set(links);
    console.log ("Nueva imagen agregada");
    clearFormInputs();
  }

  const clearFormInputs = () => {
    setValues({...initialFormValues});
  };

  return(
   <div className="container p-4">
            { user ? (<ImageForm handleLogout={handleLogout} handleSubmit={handleSubmit} values={values} setValues={setValues} handleInputChange={handleInputChange}/>)
            :(card ? <Login 
              changeCardState={changeCardState}
              handleLogin={handleLogin} 
              email={email} 
              setEmail={setEmail} 
              password={password} 
              setPassword={setPassword}
              emailError={emailError}
              passwordError={passwordError}  /> 
              : <Register 
              changeCardState={changeCardState}
              handleRegister={handleRegister}
              email={email} 
              setEmail={setEmail} 
              password={password} 
              setPassword={setPassword}
              emailError={emailError}
              passwordError={passwordError}/>)
              }
   </div>
    );
}

export default App;
