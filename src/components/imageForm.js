import React from 'react';

const ImageForm = (props)=>{
  const {
    handleLogout, 
    handleSubmit,
    handleInputChange,
    values,
    setValues
  }=props;
  return(
    <div>
<nav className="navbar navbar-expand-lg navbar-dark bg-dark mb-4">

<b><i className="navbar-brand">Bienvenido</i></b>
<button className="btn btn-danger my-2 my-sm-0 ml-auto mr-1 " type="button" onClick={handleLogout}>Cerrar Sesión</button>


</nav>
<div className="card card-body">
    <div className="card-header bg-success mb-3" ><h4>Ingresa tu imagen favorita</h4></div>
    <div className="card card-body">
      <div className="form">
        <div className="form-group">
        <label htmlFor="url">Url de Imagen</label>
          <input  className="form-control" type="url" name="urlImagen" placeholder="Ingresa la url de tu imagen favorita" onChange={handleInputChange} value = {values.urlImagen}></input>
          <p className="text-danger"></p>
        </div>
        <div className="form-group">
          <label htmlFor="password">Descripcion</label>
          <textarea className="form-control" type="text" name="descripcion" placeholder="Descripción de la imagen" onChange={handleInputChange} value = {values.descripcion} ></textarea>
          <p className="text-danger" ></p>
        </div>
        <div className="card-footer">
          <button onClick={handleSubmit} type="button" className="btn btn-success btn-block" >
            Guardar
          </button>
         
        </div>
      </div>
    </div>
    </div>
</div>

  
  );
};

export default ImageForm;
