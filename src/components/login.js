import React from "react";

const Login = (props) => {

  const {changeCardState,
  handleLogin,
  email, 
  setEmail,
  password, 
  setPassword,
  emailError,
  passwordError} = props;
  
    return (
      <div className = "card card-body">
          <div className="card-header bg-primary mb-3" ><h4>Login</h4></div>
          <div className="card card-body">
            <div className="form">
              <div className="form-group">
                <label htmlFor="mail">Correo</label>
                <input className="form-control" type="email" name="mail" placeholder="Ej: abc@gmail.com" value={email} onChange={(e)=> setEmail(e.target.value)}></input>
                <p className="text-danger" >{emailError}</p>
              </div>
              <div className="form-group">
                <label htmlFor="password">Contraseña</label>
                <input className="form-control" type="password" name="password" placeholder="Ingresa tu clave" value={password} onChange={(e)=> setPassword(e.target.value)}></input>
                <p className="text-danger" >{passwordError}</p>
              </div>
              <div className="card-footer">
                <button type="button" className="btn btn-primary btn-block" onClick={handleLogin}>
                  Iniciar sesión
                </button>
               
              </div>
              <button className="btn btn-link text-info" onClick={changeCardState}>Regístrate</button>
            </div>
          </div>
      </div>
    );
      
  
}

export default Login;